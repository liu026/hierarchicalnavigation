﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HNavigation
{
    public partial class SurprisePage : ContentPage
    {
        public SurprisePage(string LabelOfSuprise)
        {
            InitializeComponent();

            SupriseLabel.Text = LabelOfSuprise;
        }
    }
}

﻿using System.Diagnostics;
using Xamarin.Forms;
using System;

namespace HNavigation
{
    public partial class HNavigationPage : ContentPage
    {
        public HNavigationPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HNavigationPage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void OnClickme(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnClickme)}");

            bool UsersResponse = await DisplayAlert("Alert!",
                                                "Are you sure you want to click this button?",
                                                "YES",
                                                "NO");
            if(UsersResponse == true)
            {
                await Navigation.PushAsync(new SurprisePage("Surprise!"));
            }
        }
    }
}
